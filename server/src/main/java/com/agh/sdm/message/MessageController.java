package com.agh.sdm.message;

/**
 * Created by DAMIAN
 * on 08.04.14.
 */

import com.tangosol.net.NamedCache;
import common.sdm.message.Credentials;
import common.sdm.message.Envelope;
import common.sdm.message.Message;
import common.sdm.message.ResponseStatus;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

@Controller
@SuppressWarnings("unchecked")
public class MessageController {

    private Logger logger = Logger.getLogger(MessageController.class);

    private static final java.lang.Object PRIMARY_ID_NAME = "primaryId";
    private static final long SESSION_TIME_TO_EXPIRE_MS = 300000; // 5 minutes

    // Distributed Caches
    @Resource(name = "messageCache")
    private NamedCache messageCache;

    @Resource(name = "userToMessageIdsCache")
    private NamedCache userToMessageIdsCache;

    @Resource(name = "primaryIdCache")
    private NamedCache primaryIdCache;

    @Resource(name = "registeredUsers")
    private NamedCache registeredUsers;

    @Resource(name = "sessionTokenCache")
    private NamedCache sessionTokenCache;

    public MessageController() {
//        CacheFactory.ensureCluster();
        /*messageCache = CacheFactory.getCache("messageCache");
        userToMessageIdsCache = CacheFactory.getCache("userToMessageIdsCache");
        primaryIdCache = CacheFactory.getCache("primaryIdCache");
        registeredUsers = CacheFactory.getCache("registeredUsers");
        sessionTokenCache = CacheFactory.getCache("sessionTokenCache");*/
    }

    // Security random for generating security token
    private SecureRandom secureRandom = new SecureRandom();

    @RequestMapping(value = "login", method = RequestMethod.POST)
    @ResponseBody
    public String login(@RequestBody Credentials credentials) {
        String username = credentials.getUsername();
        String password = credentials.getPassword();
        registeredUsers.lock(username, -1);
        try {
            String registeredPassword = (String) registeredUsers.get(username);
            if (registeredPassword == null || !registeredPassword.equals(password)) {
                if (registeredPassword == null) {
                    logger.info("Login failed. Username: " + username + " doesn't exist.");
                } else {
                    logger.info("Login failed. Username: " + username + " sent incorrect password.");
                }
                return ResponseStatus.FAIL.toString();
            } else {
                String token = generateToken();
                updateSessionTimeToExpire(token, username);
                logger.info("Login successful for username: " + username + ". Generating new token: " + token);
                return token;
            }
        } finally {
            registeredUsers.unlock(username);
        }
    }

    private String generateToken() {
        return new BigInteger(130, secureRandom).toString(32);
    }

    @RequestMapping(value = "register", method = RequestMethod.POST)
    @ResponseBody
    public String register(@RequestBody Credentials credentials) {
        String username = credentials.getUsername();
        String password = credentials.getPassword();
        registeredUsers.lock(username, -1);
        try {
            if (registeredUsers.get(username) != null) {
                logger.info("Registration failed. Username: " + username + " already exists.");
                return ResponseStatus.FAIL.toString();
            } else {
                logger.info("Registering new user with username: " + username);
                registeredUsers.put(username, password);
                return ResponseStatus.OK.toString();
            }
        } finally {
            registeredUsers.unlock(username);
        }
    }

    @RequestMapping(value = "getMessages/" + "{senderName}", method = RequestMethod.POST)
    @ResponseBody
    public List<Message> getMessages(@PathVariable String senderName, @RequestBody String token) {
        if (!isTokenValid(token, senderName)) {
            logger.info("Attempting to get messages by not authorized user for user: " + senderName);
            return null;
        }
        logger.info("Returning all messages for client: " + senderName);

        List<Integer> messageIds = (List<Integer>) userToMessageIdsCache.get(senderName);

        if (messageIds == null) {
            logger.info("There are no messages for client: " + senderName);
            messageIds = Collections.EMPTY_LIST;
        }

        return getCachedMessages(messageIds);
    }

    private List<Message> getCachedMessages(List<Integer> messageIds) {
        List<Message> cachedMessages = new LinkedList<>();
        for (Integer messageId : messageIds) {
            Message cachedMessage = (Message) messageCache.get(messageId);
            if (cachedMessage != null) {
                cachedMessages.add(cachedMessage);
            }
        }
        return cachedMessages;
    }

    @RequestMapping(value = "sendMessage", method = RequestMethod.POST)
    @ResponseBody
    public String addMessage(@RequestBody Envelope envelope) {
        String token = envelope.getToken();
        Message message = envelope.getMessage();
        String senderName = message.getSender();
        if (!isTokenValid(token, senderName)) {
            logger.info("Attempting to add message by not authorized user for username: " + senderName);
            return ResponseStatus.FAIL.toString();
        }

        String recipient = message.getRecipient();

        Long validTime = message.getValidTime();
        logger.info("Adding message from: " + senderName + ", "
                + "to: " + recipient + ", "
                + "sent: " + message.getSentTime() + ", "
                + "with valid time: " + validTime + ", "
                + "with content: " + message.getContent());

        List<Integer> messageIds = (LinkedList<Integer>) userToMessageIdsCache.get(recipient);

        if (messageIds == null) {
            logger.info("Creating message ID list in cache for client: " + recipient);
            messageIds = new LinkedList<>();
        }

        Integer newMessageId = getUniqueMessageId();
        logger.info("Generated new message ID: " + newMessageId);

        messageIds.add(newMessageId);
        logger.info("Putting message ID list in cache with validTime: " + validTime);
        messageCache.put(newMessageId, message, validTime);
        userToMessageIdsCache.put(recipient, messageIds);
        return ResponseStatus.OK.toString();
    }

    private boolean isTokenValid(String token, String username) {
        sessionTokenCache.lock(username, -1);
        try {
            String persistedToken = (String) sessionTokenCache.get(username);
            if (persistedToken == null || !persistedToken.equals(token)) {
                logger.info("Persisted token:" + persistedToken + ", but got token: " + token);
                return false;
            } else {
                updateSessionTimeToExpire(token, username);
                return true;
            }
        } finally {
            sessionTokenCache.unlock(username);
        }
    }

    private void updateSessionTimeToExpire(String token, String username) {
        sessionTokenCache.put(username, token, SESSION_TIME_TO_EXPIRE_MS);
    }

    private Integer getUniqueMessageId() {
        Integer newMessageId = -1;
        primaryIdCache.lock(PRIMARY_ID_NAME, -1);
        try {
            newMessageId = (Integer) primaryIdCache.get(PRIMARY_ID_NAME);
            logger.info("Getting message ID from cache with value: " + newMessageId);
            if (newMessageId == null) {
                newMessageId = 0;
            }
            newMessageId++;
            primaryIdCache.put(PRIMARY_ID_NAME, newMessageId);
        } finally {
            primaryIdCache.unlock(PRIMARY_ID_NAME);
        }
        return newMessageId;
    }
}