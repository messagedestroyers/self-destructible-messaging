package com.agh.sdm.message

import com.tangosol.net.NamedCache
import common.sdm.message.Credentials
import common.sdm.message.Envelope
import common.sdm.message.Message
import common.sdm.message.ResponseStatus
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import spock.lang.Specification

import static org.mockito.Mockito.*

/**
 * Created:    2014-06-10<br>
 * Copyright:  Copyright (c) 2014<br>
 *
 * @author Nikodem Karbowy<br>
 */
class MessageControllerTest extends Specification {

    @InjectMocks
    private MessageController controller

    @Mock
    private NamedCache messageCache

    @Mock
    private NamedCache userToMessageIdsCache

    @Mock
    private NamedCache primaryIdCache

    @Mock
    private NamedCache registeredUsers

    @Mock
    private NamedCache sessionTokenCache

    def setup() {
        MockitoAnnotations.initMocks(this)
    }

    def "Should register new user when his username is available"() {
        given:
        def username = "Adriano"
        def password = "Italiano"
        def credentials = new Credentials(username, password)

        when:
        def result = controller.register(credentials)

        then:
        result == "OK"
        and:
        {
            verify(registeredUsers).put(username, password)
        }
    }

    def "Should not register new user when his username already taken"() {
        given:
        def username = "Adriano"
        def password = "Italiano"
        def credentials = new Credentials(username, password)
        and:
        when(registeredUsers.get(username)).thenReturn(password)

        when:
        def responseStatus = controller.register(credentials)

        then:
        responseStatus == ResponseStatus.FAIL.toString()
        and:
        {
            verify(registeredUsers, never()).put(username, password)
        }
    }

    def "Should generate session token for already registered user"() {
        given:
        def username = "Adriano"
        def password = "Italiano"
        def credentials = new Credentials(username, password)
        and:
        when(registeredUsers.get(username)).thenReturn(password)

        when:
        def responseStatus = controller.login(credentials)

        then:
        responseStatus != ResponseStatus.FAIL.toString()
    }

    def "Should not generate session token for not registered user"() {
        given:
        def username = "Adriano"
        def password = "Italiano"
        def credentials = new Credentials(username, password)

        when:
        def responseStatus = controller.login(credentials)

        then:
        responseStatus == ResponseStatus.FAIL.toString()
    }

    def "Should return null when user with invalid token asks for messages"() {
        given:
        def username = "Adriano"
        def token = "8faaasbk6oci2p3gqrr1l6q1cu"
        def messageId = 1
        def message = new Message("Sender", username, "Topic", "Content", 12345, 12345)
        and:
        when(userToMessageIdsCache.get(username)).thenReturn([messageId])
        when(messageCache.get(messageId)).thenReturn(message)

        when:
        def messages = controller.getMessages(username, token)

        then:
        messages == null
    }

    def "Should return message list when user with valid token asks for them"() {
        given:
        def username = "Adriano"
        def token = "8faaasbk6oci2p3gqrr1l6q1cu"
        def messageId = 1
        def message = new Message("Sender", username, "Topic", "Content", 12345, 12345)
        and:
        when(sessionTokenCache.get(username)).thenReturn(token)
        when(userToMessageIdsCache.get(username)).thenReturn([messageId])
        when(messageCache.get(messageId)).thenReturn(message)

        when:
        def messages = controller.getMessages(username, token)

        then:
        messages == [message]
    }

    def "Should add new message for user with valid token"() {
        given:
        def sender = "Adriano"
        def recipient = "Damiano"
        def validTime = 12345
        def message = new Message(sender, recipient, "Topic", "Content", 54321, validTime)
        def token = "8faaasbk6oci2p3gqrr1l6q1cu"
        def envelope = new Envelope(message, token)
        and:
        when(sessionTokenCache.get(sender)).thenReturn(token)

        when:
        def resultStatus = controller.addMessage(envelope)

        then:
        resultStatus == ResponseStatus.OK.toString()
        and:
        {
            def messageId = 1
            verify(userToMessageIdsCache).put(recipient, [messageId])
            verify(messageCache).put(messageId, message, validTime)
        }
    }

    def "Should not add new message for user with invalid token"() {
        given:
        def username = "Adriano"
        def message = new Message(username, "Recipient", "Topic", "Content", 12345, 12345)
        def token = "8faaasbk6oci2p3gqrr1l6q1cu"
        def envelope = new Envelope(message, "blablablablabla")
        and:
        when(sessionTokenCache.get(username)).thenReturn(token)

        when:
        def resultStatus = controller.addMessage(envelope)

        then:
        resultStatus == ResponseStatus.FAIL.toString()
    }

}
