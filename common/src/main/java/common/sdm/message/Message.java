package common.sdm.message;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Message implements Serializable {
    private static final long serialVersionUID = 123235436345645L;

    private transient long id;
    private String sender;
    private String recipient;
    private String topic;
    private String content;
    private Long sentTime;
    private Long validTime;

    public Message() {
    }

    public Message(String sender, String recipient, String topic, String content, Long sentTime, Long validTime) {
        this.sender = sender;
        this.recipient = recipient;
        this.topic = topic;
        this.content = content;
        this.sentTime = sentTime;
        this.validTime = validTime;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public String getSender() {
        return sender;
    }

    public String getRecipient() {
        return recipient;
    }

    public String getTopic(){
        return topic;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public void setRecipient(String recipient) {
        this.recipient = recipient;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Long getSentTime() {
        return sentTime;
    }

    public void setSentTime(Long sentTime) {
        this.sentTime = sentTime;
    }

    public Long getValidTime() {
        return validTime;
    }

    public void setValidTime(Long validTime) {
        this.validTime = validTime;
    }

    public boolean isToEvict() {
        return sentTime + validTime < System.currentTimeMillis();
    }

    public String getSentDate() {
        return new SimpleDateFormat("dd.MM.yyyy 'at' HH:mm:ss").format(new Date(sentTime));
    }

    public int getValidThroughInMin() {
        return (int)(sentTime + validTime - System.currentTimeMillis()) / (1000 * 60);
    }

    public long getValidThroughInMs() {
        return sentTime + validTime - System.currentTimeMillis();
    }

    @Override
    public String toString() {
        return "Message{" +
                "id=" + id +
                ", sender='" + sender + '\'' +
                ", recipient='" + recipient + '\'' +
                ", topic='" + topic + '\'' +
                ", content='" + content + '\'' +
                ", sentTime=" + sentTime +
                ", validTime=" + validTime +
                '}';
    }
}
