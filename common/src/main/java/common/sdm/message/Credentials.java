package common.sdm.message;

import java.io.Serializable;

/**
 * Created:    2014-05-27<br>
 * Copyright:  Copyright (c) 2014<br>
 *
 * @author Nikodem Karbowy<br>
 */
public class Credentials implements Serializable {

    private String username;
    private String password;

    public Credentials() {
    }

    public Credentials(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
