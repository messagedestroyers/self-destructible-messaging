package common.sdm.message;

/**
 * Created:    2014-05-27<br>
 * Copyright:  Copyright (c) 2014<br>
 *
 * @author Nikodem Karbowy<br>
 */
public enum ResponseStatus {
    FAIL, OK
}
