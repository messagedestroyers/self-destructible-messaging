package common.sdm.message;

import java.io.Serializable;

/**
 * Created:    2014-05-27<br>
 * Copyright:  Copyright (c) 2014<br>
 *
 * @author Nikodem Karbowy<br>
 */
public class Envelope implements Serializable {

    private Message message;
    private String token;

    public Envelope() {
    }

    public Envelope(Message message, String token) {
        this.message = message;
        this.token = token;
    }

    public Message getMessage() {
        return message;
    }

    public void setMessage(Message message) {
        this.message = message;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
